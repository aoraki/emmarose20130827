package com.aspect.techtest.exceptions;

/**
 * Exception to be thrown for Bad Requests
 *
 * Created by JohnRK on 18/06/2016.
 */

public class BadRequestException extends RuntimeException implements AspectException
{

    public BadRequestException(){
        super();
    }

    public BadRequestException(String message){
        super(message);
    }

    public BadRequestException(String message, Throwable cause) { super(message, cause); }

    public BadRequestException(Throwable cause) {
        super(cause);
    }
}