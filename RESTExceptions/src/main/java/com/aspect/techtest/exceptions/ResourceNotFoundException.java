package com.aspect.techtest.exceptions;

/**
 * Exception that can be thrown in situations when the specified resource cannot be found
 *
 * Created by JohnRK on 18/06/2016.
 */
public class ResourceNotFoundException extends  RuntimeException implements AspectException {

    private String field;
    private Object value;

    public ResourceNotFoundException(String field, Object value){
        this.field = field;
        this.value = value;
    }

    public String getField(){
        return field;
    }

    public Object getValue(){
        return value;
    }
}