package com.aspect.techtest.exceptions;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


/**
 * Created by JohnRK on 18/06/2016.
 *
 * Exception handler for various scenarios
 *
 */
@ControllerAdvice()
public class AspectExceptionHandler {

    public AspectExceptionHandler() {
    }

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String EXCEPTION_HANDLER ="Aspect Exception Handler";



    /**
     * Return 500 Catch all Exception Handler
     *
     * Triggered for Uncaught exceptions
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @Order(Ordered.LOWEST_PRECEDENCE)
    public ServerUnexpectedErrorResponse handleAnyUncaughtSituation(HttpServletRequest req, Throwable e){

        String errorURL = req.getRequestURL().toString();

        DateTime timeStamp = DateTime.now();
        logger.error(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.INTERNAL_SERVER_ERROR + ", URL : " + errorURL + ", Error was:", e);

        StringBuilder errorStringBuilder = new StringBuilder();

        errorStringBuilder.append("Ooops!  This is not good.  An unforseen error has occurred in the Work Order System");
        errorStringBuilder.append("\nPlease try again later, the issue may be a temporary one.");
        errorStringBuilder.append("\nIf the issue persists please contact the system administrator");
        errorStringBuilder.append("\n\nRequest URL : ");
        errorStringBuilder.append(errorURL);
        errorStringBuilder.append("\n\nError Timestamp : ");
        errorStringBuilder.append(timeStamp.toString());

        return new ServerUnexpectedErrorResponse(errorURL, errorStringBuilder.toString(), timeStamp.toString());
    }

    /**
     * Convenience class to contain details of Server Unexpected Errors
     */
    protected class ServerUnexpectedErrorResponse {
        public final String url;
        public final String errorMessage;
        public final String timeStamp;

        public ServerUnexpectedErrorResponse(String url, String errorMessage, String timeStamp) {
            this.url = url;
            this.errorMessage = errorMessage;
            this.timeStamp = timeStamp;
        }
    }



    /**
     * Return 404 - Not Found
     * When an attempt to read an entity that does not exist is made
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResourceIssueResponse handleResourceNotFound(HttpServletRequest req, ResourceNotFoundException ex) {

        String requestURL = req.getRequestURL().toString();

        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.NOT_FOUND + ", URL : " + requestURL + ", Error was:", ex.getMessage());
        ResourceIssueResponse response = new ResourceIssueResponse(requestURL, ex.getField(), ex.getValue());
        return response;
    }

    /**
     * Convenience class to contain details of resource issues
     */
    protected class ResourceIssueResponse{

        public final String url;
        public final String field;
        public final Object value;

        public ResourceIssueResponse(String url, String field, Object value){
            this.url =url;
            this.field = field;
            this.value = value;
        }
    }

    /**
     * Should be thrown when an attempt is made to create a resource that already exists
     *
     * Return HTTP CONFLICT 409 When an attempt to create a resource that already exists is made
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(ResourceAlreadyExistsException.class)
    @ResponseStatus(value= HttpStatus.CONFLICT)
    @ResponseBody
    public ResourceIssueResponse handleResourceAlreadyExists(HttpServletRequest req, ResourceAlreadyExistsException ex) {
        String requestURL = req.getRequestURL().toString();
        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.CONFLICT + ", URL : " + requestURL + ", Error was:", ex.getMessage());
        ResourceIssueResponse response = new ResourceIssueResponse(requestURL, ex.field, ex.value);
        return response;
    }


    /**
     * Return 403
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value= HttpStatus.FORBIDDEN)
    @ResponseBody
    public ForbiddenValueResponse handleMethodArgumentNotValid(HttpServletRequest req, MethodArgumentNotValidException ex) {

        String requestURL = req.getRequestURL().toString();

        List<ErrorField> fields  =
                ex.getBindingResult().getFieldErrors()
                        .stream()
                        .map(error ->
                                {
                                    String field = error.getField();
                                    Object value = error.getRejectedValue();
                                    String message = error.getCode();
                                    return new ErrorField(field, value, message);
                                }
                        ).collect(Collectors.toList());

        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.FORBIDDEN + ", URL : " + requestURL + ", Error was:", ex.getMessage());
        ForbiddenValueResponse response = new ForbiddenValueResponse(requestURL, fields);

        return response;
    }

    public class ForbiddenValueResponse{

        public final String url;
        public final List<ErrorField> field;

        public ForbiddenValueResponse(String url, List<ErrorField> field){
            this.url =url;
            this.field = field;
        }
    }

    public class ErrorField{

        public final String field;
        public final Object errorValue;
        public final String message;

        public ErrorField(String field, Object errorValue, String message){
            this.field =field;

            if(errorValue == null){
                this.errorValue = "";
            }
            else{
                this.errorValue=errorValue;
            }
            this.message=message;
        }
    }

    /**
     * HttpStatus.BAD_REQUEST
     *
     * For Bad message
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionMessageResponse handleHttpMessageNotReadable(HttpServletRequest req, HttpMessageNotReadableException ex) {

        String requestURL = req.getRequestURL().toString();
        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.BAD_REQUEST + ", URL : " + requestURL + ", Unreadable HTTP Request", ex.getMessage());
        return new ExceptionMessageResponse("Unreadable HTTP Request : " + ex.getMessage());
    }

    /**
     * HttpStatus.BAD_REQUEST
     *
     * For Bad message
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionMessageResponse handleMissingServletRequestParameter(HttpServletRequest req, MissingServletRequestParameterException ex) {

        String requestURL = req.getRequestURL().toString();
        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.BAD_REQUEST + ", URL : " + requestURL + ", Missing servlet request parameter", ex.getMessage());
        return new ExceptionMessageResponse("Missing servlet request parameter : " + ex.getMessage());
    }


    /**
     * HttpStatus.BAD_REQUEST
     *
     * For Bad message
     *
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionMessageResponse handleHttpRequestMethodNotSupported(HttpServletRequest req, HttpRequestMethodNotSupportedException ex) {

        String requestURL = req.getRequestURL().toString();
        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.BAD_REQUEST + ", URL : " + requestURL + ", HTTP Request Method not supported", ex.getMessage());
        return new ExceptionMessageResponse(ex.getMessage() + ", URL : " + requestURL);
    }

    /**
     * Return 400 when invalid requests content
     * @param req
     * @param ex
     * @return
     */
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionMessageResponse handleBadRequest(HttpServletRequest req, BadRequestException ex) {
        String requestURL = req.getRequestURL().toString();
        logger.info(EXCEPTION_HANDLER + ": Error Code = " + HttpStatus.BAD_REQUEST + ", URL : " + requestURL + ", HTTP Request Invalid", ex.getMessage());
        return new ExceptionMessageResponse("HTTP Request Invalid : " + ex.getMessage());
    }

    public class ExceptionMessageResponse {
        public final String errorMessage;

        public ExceptionMessageResponse(String errorMessage) {
            this.errorMessage = errorMessage;
        }
    }
}
