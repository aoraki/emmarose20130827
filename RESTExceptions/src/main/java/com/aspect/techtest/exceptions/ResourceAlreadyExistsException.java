package com.aspect.techtest.exceptions;

/**
 * Exception that can be thrown in situations when a resource already exists
 *
 * Created by JohnRK on 18/06/2016.
 */
public class ResourceAlreadyExistsException  extends  RuntimeException implements AspectException {

    public final String field;
    public final Object value;

    public ResourceAlreadyExistsException(final String field, final Object value){
        this.field = field;
        this.value = value;
    }

}