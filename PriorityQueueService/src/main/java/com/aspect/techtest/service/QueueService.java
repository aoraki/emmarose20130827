package com.aspect.techtest.service;

import com.aspect.techtest.exceptions.BadRequestException;
import com.aspect.techtest.persistence.QueueType;
import com.aspect.techtest.persistence.WorkOrderQueue;
import com.aspect.techtest.persistence.WorkOrderQueueFactory;
import com.aspect.techtest.persistence.resources.WorkOrder;
import com.aspect.techtest.persistence.resources.WorkOrderBuilder;
import com.aspect.techtest.persistence.resources.WorkOrderType;
import com.aspect.techtest.persistence.utils.WorkOrderHelper;
import org.joda.time.DateTime;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by JohnRK on 14/06/2016.
 */
public class QueueService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private WorkOrderQueueFactory queueFactory;
    private WorkOrderQueue workOrderQueue;

    DateTimeFormatter patternFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");


    public QueueService(WorkOrderQueueFactory queueFactory, QueueType type) {
        log.debug("Initialising QueueService");
        this.queueFactory = queueFactory;
        this.workOrderQueue = this.queueFactory.buildQueue(type);
    }


    public boolean enqueueWorkOrder(String requestorId, String timestamp) throws Exception {
        DateTime dateSubmitted;
        long requestorIdLong = 0;

        if(!this.validateRequestorId(requestorId).equals("")){
            throw new BadRequestException(this.validateRequestorId(requestorId));
        }

        if(!this.validateTimestamp(timestamp).equals("")){
            throw new BadRequestException(this.validateTimestamp(timestamp));
        }

        try{
            requestorIdLong = Long.parseLong(requestorId);
        } catch(Exception ex){
            throw new BadRequestException("Error parsing requestorId");
        }

        try{
            dateSubmitted = DateTime.parse(timestamp, patternFormat);
        } catch (Exception ex){
            throw new BadRequestException("Error parsing date time");
        }

        dateSubmitted = DateTime.parse(timestamp, patternFormat);

        WorkOrderType type = WorkOrderHelper.determineTypeBasedOnID(requestorIdLong);
        int mgmtOverride = type.equals(WorkOrderType.ManagementOverride) ? 1 : 0;


        return workOrderQueue.enqueueWorkOrder(
                new WorkOrderBuilder()
                        .setRequestorId(requestorIdLong)
                        .setDateSubmitted(dateSubmitted)
                        .setIsMgmtOveride(mgmtOverride)
                        .setWorkOrderType(type)
                        .createWorkOrder());
    }

    public WorkOrder dequeue() throws Exception {
        return workOrderQueue.dequeueWorkOrder();
    }

    public List<WorkOrder> getSortedWorkOrderList() throws Exception {
        return workOrderQueue.getSortedWorkOrderList();
    }

    public Boolean removeWorkOrder(String requestorId) throws Exception {
        long requestorIdLong = 0;
        if(!this.validateRequestorId(requestorId).equals("")){
            throw new BadRequestException(this.validateRequestorId(requestorId));
        }

        try{
            requestorIdLong = Long.parseLong(requestorId);
        } catch(Exception ex){
            log.debug("Error parsing requestorId");
        }

        return workOrderQueue.removeWorkOrder(new WorkOrderBuilder().setRequestorId(requestorIdLong).createWorkOrder());
    }

    public long getPositionOfID(String requestorId) throws Exception {
        long requestorIdLong = 0;

        if(!this.validateRequestorId(requestorId).equals("")){
            throw new BadRequestException(this.validateRequestorId(requestorId));
        }

        try{
            requestorIdLong = Long.parseLong(requestorId);
        } catch(Exception ex){
            log.debug("Error parsing requestorId");
        }

        return workOrderQueue.getPositionOfID(new WorkOrderBuilder().setRequestorId(requestorIdLong).createWorkOrder());
    }

    public long getAverageWaitTime(String currentTime) throws Exception{
        DateTime dateSubmitted;
        if(!this.validateTimestamp(currentTime).equals("")){
            throw new BadRequestException(this.validateTimestamp(currentTime));
        }

        try{
            dateSubmitted = DateTime.parse(currentTime, patternFormat);
        } catch (Exception ex){
            throw new BadRequestException("Error parsing date time");
        }

        return workOrderQueue.getAverageWaitTime(dateSubmitted);
    }


    /**
     * Method to ensure that the requestor Id falls into the range of values
     * 1 and 9223372036854775807.
     *
     * @param requestorId
     * @return
     */
    private String validateRequestorId(String requestorId){
        String errorMessage = "";
        try{
            long requestorIdLong = Long.parseLong(requestorId);
            if(requestorIdLong < 1){
                errorMessage = "Requestor Id must be a value between 1 and 9223372036854775807.";
            }
        } catch (Exception ex){
            errorMessage = "Requestor Id must be a value between 1 and 9223372036854775807.";
        }
        return errorMessage;
    }

    /**
     * Method to ensure that the timestamp string that is passed into the controller
     * can be parsed into a valid date.  It not an error should be returned
     *
     * @param timestamp
     * @return
     */
    private String validateTimestamp(String timestamp){
        String errorMessage = "";
        DateTime now = DateTime.now();
        try{
            DateTime dt = DateTime.parse(timestamp, patternFormat);
            if(dt.isAfter(now)){
                return "You cannot specify a timestamp in the future";
            }
        } catch (Exception ex){
            return "The timestamp must be a date that follows the following format : yyyy-MM-dd'T'HH:mm:ss";
        }
        return errorMessage;
    }
}
