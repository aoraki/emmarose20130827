package com.aspect.techtest;

/**
 * The application entry point.  Contains the main method of the spring boot application
 *
 * Created by JohnRK on 14/06/2016.
 */
import com.aspect.techtest.exceptions.AspectExceptionHandler;
import com.aspect.techtest.persistence.QueueType;
import com.aspect.techtest.persistence.WorkOrderQueueFactory;
import com.aspect.techtest.service.QueueService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;


@SpringBootApplication
public class Application {

    private static ApplicationContext ctx;
    public static void main(String[] args) {
        ctx = SpringApplication.run(Application.class, args);
    }

    @Bean
    @Scope("singleton")
    public AspectExceptionHandler aspectExceptionHandler() throws Exception {
        return new AspectExceptionHandler();
    }

    @Bean
    @Scope("singleton")
    public static WorkOrderQueueFactory getWorkOrderQueueFactory()  throws Exception {
        return new WorkOrderQueueFactory();
    }

    @Bean
    @Scope("singleton")
    public static QueueService queueService() throws Exception {
        return new QueueService(getWorkOrderQueueFactory(), QueueType.INMEMORY);
    }
}
