package com.aspect.techtest.endpoint;

import com.aspect.techtest.persistence.resources.WorkOrder;
import com.aspect.techtest.service.QueueService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by johnrk on 17-Jun-16.
 */
@RestController
@RequestMapping("/priorityQueue")
public class PriorityQueueController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final static String PARAM_REQUESTORID = "requestorId";
    private final static String TIMESTAMP = "timestamp";
    private final static String DATEFORMAT = "yyyy-MM-dd_HH:mm:ss";

    private QueueService queueService;

    @Autowired
    public PriorityQueueController(@Qualifier("queueService") QueueService queueService) {
        this.queueService = queueService;
    }

    /**
     * An endpoint for adding a ID to queue (enqueue). This endpoint should
     * accept two parameters, the ID to enqueue and the time at which the ID
     * was added to the queue.
     *
     * Idempotent
     *
     * @param requestorId
     * @param timestamp
     * @return
     * @throws Exception
     */
    @RequestMapping(
            value = "/workOrders/{requestorId}",
            method = RequestMethod.PUT,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ResponseEntity<Boolean> enqueueWorkOrder(
            @PathVariable(value=PARAM_REQUESTORID) String requestorId,
            //@RequestParam(required = true, value = TIMESTAMP)  @DateTimeFormat(pattern=DATEFORMAT) DateTime timestamp
            @RequestParam(required = true, value = TIMESTAMP) String timestamp
    ) throws Exception {
        log.debug("Enqueue request for Requestor Id: {}, Time Stamp: {}", requestorId, timestamp);
        return new ResponseEntity<>(queueService.enqueueWorkOrder(requestorId, timestamp), HttpStatus.CREATED);
    }

    /**
     * An endpoint for getting the top ID from the queue and removing it (de-
     * queue). This endpoint should return the highest ranked ID and the time
     * it was entered into the queue.
     *
     * Idempotent
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(
            value = "/workOrders",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ResponseEntity<WorkOrder> dequeueWorkOrder(
    ) throws Exception {
        log.debug("Dequeue request received by controller");
        return new ResponseEntity<>(queueService.dequeue(), HttpStatus.OK);
    }


    /**
     * An endpoint for getting the top ID from the queue and removing it (de-
     * queue). This endpoint should return the highest ranked ID and the time
     * it was entered into the queue.
     *
     * Non-Idempotent
     *
     * @return
     * @throws Exception
     */

    @RequestMapping(
            value = "/workOrders",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ResponseEntity<List<WorkOrder>> getOrderedWorkOrderList(
    ) throws Exception {
        log.debug("Getting sorted work order list");
        return new ResponseEntity<>(queueService.getSortedWorkOrderList(), HttpStatus.OK);
    }


    /**
     * An endpoint for removing a specific ID from the queue. This endpoint
     * should accept a single parameter, the ID to remove.
     *
     * Idempotent
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(
            value = "/workOrders/{requestorId}",
            method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ResponseEntity<Boolean> removeWorkOrder(
            @PathVariable(value=PARAM_REQUESTORID) String requestorId
    ) throws Exception {
        log.debug("Remove work order request for Requestor Id: {}", requestorId);
        return new ResponseEntity<>(queueService.removeWorkOrder(requestorId), HttpStatus.OK);
    }


    /**
     * An endpoint to get the position of a specific ID in the queue. This endpoint
     * should accept one parameter, the ID to get the position of. It should return
     * the position of the ID in the queue indexed from 0.
     *
     * Non-Idempotent
     *
     * @param requestorId
     * @return
     * @throws Exception
     */
    @RequestMapping(
            value = "/workOrders/{requestorId}",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ResponseEntity<Long> getWorkOrderIndex(
            @PathVariable(value=PARAM_REQUESTORID) String requestorId
    ) throws Exception {
        log.debug("Get Index of work order for Requestor Id: {}", requestorId);
        return new ResponseEntity<>(queueService.getPositionOfID(requestorId), HttpStatus.OK);
    }


    /**
     * An endpoint to get the average wait time. This endpoint should accept a
     * single parameter, the current time, and should return the average (mean)
     * number of seconds that each ID has been waiting in the queue.
     *
     * Non-Idempotent
     *
     * @param timestamp
     * @return
     * @throws Exception
     */

    @RequestMapping(
            value = "/workOrders/time",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public @ResponseBody
    ResponseEntity<Long> getAverageWorkOrderWaitingTime(
            @RequestParam(required = true, value = TIMESTAMP) String timestamp
    ) throws Exception {
        log.debug("Getting the average waiting time for the work orders in the queue,  Current time is {}", timestamp);
        return new ResponseEntity<>(queueService.getAverageWaitTime(timestamp), HttpStatus.OK);
    }
}
