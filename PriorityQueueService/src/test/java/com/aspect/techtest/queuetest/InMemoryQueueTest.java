package com.aspect.techtest.queuetest;

import com.aspect.techtest.persistence.impl.InMemoryWorkOrderQueue;
import com.aspect.techtest.persistence.resources.WorkOrder;
import com.aspect.techtest.persistence.resources.WorkOrderBuilder;
import com.aspect.techtest.persistence.resources.WorkOrderType;
import com.aspect.techtest.persistence.utils.WorkOrderHelper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


/**
 * Unit tests to test basic operation of the In-Memory work order queue
 *
 * Created by johnrk on 19-Jun-16.
 */
public class InMemoryQueueTest {

    private InMemoryWorkOrderQueue imwoqTest = new InMemoryWorkOrderQueue();

    private WorkOrder woEnqueueTest;

    @Before
    public void setUp() throws Exception {
        // Create a number of work order objects and then enqueue them in the
        DateTime dtSubmitted = DateTime.now().minusHours(1);

        // Work Order 1
        WorkOrder wo1 = this.constructWorkOrder(1, dtSubmitted);
        WorkOrder wo2 = this.constructWorkOrder(3, dtSubmitted);
        WorkOrder wo3 = this.constructWorkOrder(5, dtSubmitted);
        WorkOrder wo4 = this.constructWorkOrder(15, dtSubmitted);

        woEnqueueTest = this.constructWorkOrder(20, dtSubmitted);

        this.imwoqTest.enqueueWorkOrder(wo1);
        this.imwoqTest.enqueueWorkOrder(wo2);
        this.imwoqTest.enqueueWorkOrder(wo3);
        this.imwoqTest.enqueueWorkOrder(wo4);

    }

    private WorkOrder constructWorkOrder(long requestorId, DateTime dateSubmitted){
        WorkOrderType type = WorkOrderHelper.determineTypeBasedOnID(requestorId);
        int mgmtOverride = type.equals(WorkOrderType.ManagementOverride) ? 1 : 0;
        return new WorkOrderBuilder()
                .setRequestorId(requestorId)
                .setDateSubmitted(dateSubmitted)
                .setIsMgmtOveride(mgmtOverride)
                .setWorkOrderType(type)
                .createWorkOrder();
    }

    @Test
    public void testCorrectNumberOfElementsInQueue() {
        try {
            assertTrue(this.imwoqTest.getSortedWorkOrderList().size() == 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRemoveWorkOrderID() {
        try {
            assertTrue(this.imwoqTest.removeWorkOrder(this.constructWorkOrder(1, new DateTime())) == true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEnqueueWorkOrder() {
        try {
            assertTrue(this.imwoqTest.enqueueWorkOrder(this.woEnqueueTest) == true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDequeueWorkOrder(){
        try {
            assertTrue(this.imwoqTest.dequeueWorkOrder() != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAverageWaitTimeReturnsValueForPopulatedQueue(){
        try {
            assertTrue(this.imwoqTest.getAverageWaitTime(DateTime.now()) > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetPositionOfIDReturnsValue(){
        try {
            assertTrue(this.imwoqTest.getPositionOfID(this.constructWorkOrder(5, new DateTime())) == 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEmptyQueueReturnsBlankList() {
        try {
            assertTrue(new InMemoryWorkOrderQueue().getSortedWorkOrderList().size() == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEmptyQueueReturnsZeroAverageWaitingTime() {
        try {
            assertTrue(new InMemoryWorkOrderQueue().getAverageWaitTime(DateTime.now()) == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
