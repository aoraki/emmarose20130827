package com.aspect.techtest.queuetest;

import com.aspect.techtest.endpoint.PriorityQueueController;
import com.aspect.techtest.service.QueueService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test class for the Priority Queue Rest controller
 *
 * Created by JohnRK on 20/06/2016.
 *
 */
public class PriorityQueueControllerTest {

    private MockMvc mvc;
    private final String API_ROOT_URL = "/priorityQueue";
    QueueService mockQueueService;

    //TODO Finish unit tests

    @Before
    public void setUp() throws Exception {
        mockQueueService = mock(QueueService.class);
        mvc = MockMvcBuilders.standaloneSetup(new PriorityQueueController(mockQueueService)).build();
    }

    @Test
    public void getListOfWorkOrdersCheckingContent() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(API_ROOT_URL + "/workOrders")
                .accept(MediaType.APPLICATION_JSON)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void enqueueWorkOrderValidRequestorIdAndTimestamp() throws Exception {
        mvc.perform(MockMvcRequestBuilders.put(API_ROOT_URL + "/workOrders/1?timestamp=2016-06-20T13:13:13")
                .accept(MediaType.APPLICATION_JSON)
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void getListOfWorkOrders() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(API_ROOT_URL + "/workOrders")
                .accept(MediaType.APPLICATION_JSON)
                .contentType("application/json"))
                .andExpect(status().isOk());
    }
}