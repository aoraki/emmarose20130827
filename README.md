# README #

### What is this repository for? ###

This is a code repo for a technical assignment that I have been given as a part of the selection process for a position for 
which I am a candidate.

### Repo Location ###
https://aoraki@bitbucket.org/aoraki/emmarose20130827.git

### System requirements ###
This is a spring boot application, so to run it you just need a JVM (Version 8) installed on your machine.  This can be a JRE or a JDK. 

### Building ###
The project is a gradle project, and for convenience I have bundled the gradle wrapper.
Once cloned to your local machine, navigate to the directory that the repo was cloned to, open a command terminal for that directory and type the command "gradlew build". The project will pull in any dependencies it needs at this point and the project will build.

### Running the service ###
Once built, navigate to <Repo_Dir>\PriorityQueueService\build\libs in a command terminal and run the following java command :
java -jar PriorityQueueService-0.1.0.jar

The service is configured to run on port 9090, http://localhost:9090

### Verification ###
In a browser go to http://localhost:9090/health.  A string should be displayed indicating that the service is UP.

### REST API ###
Swagger is installed in this RESTful web service.  To view the swagger ui, go to;
http://localhost:9090/swagger-ui.html

The swagger ui will display the name of the controller (priority-queue-controller).  Click on this name to see the endpoints that are available in this controller.  To view the details of an endpoint, and to test them, click on an endpoint to expand it.  You can test an endpoint by clicking the "Try it out" button for that endpoint.  If the endpoint requires parameters, you must supply the parameters it requests.

A requestor id parameter must be in the range 1 to 9223372036854775807.
A timestamp parameter must be a date-time string expressed in the following format : yyyy-MM-dd'T'HH:mm:ss

If you do not wish to use swagger you can also test it using a browser, a REST client such as Postman, or CURL.


### Who do I talk to? ###

* John Kelly (kellyjra@gmail.com)