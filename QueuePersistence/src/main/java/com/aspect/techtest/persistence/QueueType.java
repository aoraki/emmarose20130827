package com.aspect.techtest.persistence;

/**
 * An enumerated type to represent types of queues that can be instantiated by
 * the WorkOrderQueueFactory.
 *
 * Created by JohnRK on 18/06/2016.
 */
public enum QueueType {
    INMEMORY,PERSISTENT
}
