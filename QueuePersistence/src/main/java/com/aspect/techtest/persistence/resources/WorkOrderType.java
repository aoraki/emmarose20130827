package com.aspect.techtest.persistence.resources;

/**
 * Created by JohnRK on 14/06/2016.
 *
 * A enum to represent Work Order Type
 *
 */
public enum WorkOrderType {
    Normal, Priority, VIP, ManagementOverride
}
