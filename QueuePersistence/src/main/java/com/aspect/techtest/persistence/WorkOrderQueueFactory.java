package com.aspect.techtest.persistence;

import com.aspect.techtest.persistence.impl.InMemoryWorkOrderQueue;

/**
 * A Simple Factory to generate an concrete instance of a WorkOrderQueue
 * based on the QueueType that is passed in
 *
 * Created by JohnRK on 14/06/2016.
 */
public class WorkOrderQueueFactory {

    /**
     * Generates a concrete WorkOrderQueue instance based on the queue type
     *
     * @param queueType
     * @return
     */
    public static WorkOrderQueue buildQueue(QueueType queueType) {
        WorkOrderQueue queue = null;
        switch (queueType) {
            case INMEMORY:
                queue = new InMemoryWorkOrderQueue();
                break;
            case PERSISTENT:
                // Here is where we would construct a Persistent queue
                break;
            default:
                queue = new InMemoryWorkOrderQueue();
                break;
        }
        return queue;
    }
}

