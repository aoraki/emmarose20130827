package com.aspect.techtest.persistence;

import com.aspect.techtest.persistence.resources.WorkOrder;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Interface to define the behaviour of a work order queue.  This interface needs
 * to be implemented by all concrete implementations of WorkOrderQueue
 *
 * Created by JohnRK on 14/06/2016.
 */
public interface WorkOrderQueue {
    public boolean enqueueWorkOrder(WorkOrder workOrder) throws Exception;
    public WorkOrder dequeueWorkOrder() throws Exception;
    public List<WorkOrder> getSortedWorkOrderList() throws Exception;
    public boolean removeWorkOrder(WorkOrder workOrder) throws Exception;
    public long getPositionOfID(WorkOrder workOrder) throws Exception;
    public long getAverageWaitTime(DateTime currentTime) throws Exception;
}
