package com.aspect.techtest.persistence.utils;

import com.aspect.techtest.persistence.resources.WorkOrderType;

/**
 * Helper class with static methods for various utility by other classes
 *
 * Created by JohnRK on 19/06/2016.
 */
public class WorkOrderHelper {

    /**
     * Returns a WorkOrderType based on the requestor id.  The logic
     * resembles the classic Fizzbuzz coding problem.
     *
     * @param requestorId
     * @return
     */
    public static WorkOrderType determineTypeBasedOnID(long requestorId){
        if((requestorId  % 3 == 0)&&(requestorId % 5 == 0)){
            // Fizzbuzz!
            return WorkOrderType.ManagementOverride;
        } else {
            if(requestorId % 5 == 0) {
                // Buzz
                return WorkOrderType.VIP;
            } else if(requestorId % 3 == 0){
                // Fizz
                return WorkOrderType.Priority;
            } else {
                return WorkOrderType.Normal;
            }
        }
    }

}
