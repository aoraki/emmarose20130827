package com.aspect.techtest.persistence.resources;

import org.joda.time.DateTime;

public class WorkOrderBuilder {
    private long requestorId;
    private DateTime dateSubmitted;
    private double rank;
    private WorkOrderType workOrderType;
    private int isMgmtOveride;
    private int waitTimeInSeconds;

    public WorkOrderBuilder setRequestorId(long requestorId) {
        this.requestorId = requestorId;
        return this;
    }

    public WorkOrderBuilder setDateSubmitted(DateTime dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
        return this;
    }

    public WorkOrderBuilder setRank(double rank) {
        this.rank = rank;
        return this;
    }

    public WorkOrderBuilder setWorkOrderType(WorkOrderType workOrderType) {
        this.workOrderType = workOrderType;
        return this;
    }

    public WorkOrderBuilder setIsMgmtOveride(int isMgmtOveride) {
        this.isMgmtOveride = isMgmtOveride;
        return this;
    }

    public WorkOrderBuilder setWaitTimeInSeconds(int waitTimeInSeconds) {
        this.waitTimeInSeconds = waitTimeInSeconds;
        return this;
    }

    public WorkOrder createWorkOrder() {
        return new WorkOrder(requestorId, dateSubmitted, rank, workOrderType, isMgmtOveride, waitTimeInSeconds);
    }
}