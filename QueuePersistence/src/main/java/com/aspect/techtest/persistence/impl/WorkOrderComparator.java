package com.aspect.techtest.persistence.impl;

import com.aspect.techtest.persistence.resources.WorkOrder;

import java.util.Comparator;

/**
 * Created by JohnRK on 16/06/2016.
 *
 * A simple comparator to sort work orders based on the management override flag first
 * and then their rank value
 *
 * Work Orders are ranked from highest to lowest
 *
 */
public class WorkOrderComparator implements Comparator<WorkOrder> {
    public int compare(WorkOrder o1, WorkOrder o2) {
        int ret = o2.getIsMgmtOveride() - o1.getIsMgmtOveride();
        if(ret == 0){
            ret = (new Double(o2.getRank()).intValue() - new Double(o1.getRank()).intValue());
        }
        return ret;
    }
}