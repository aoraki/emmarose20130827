package com.aspect.techtest.persistence.impl;

import com.aspect.techtest.exceptions.ResourceAlreadyExistsException;
import com.aspect.techtest.exceptions.ResourceNotFoundException;
import com.aspect.techtest.persistence.WorkOrderQueue;
import com.aspect.techtest.persistence.resources.WorkOrder;
import com.aspect.techtest.persistence.resources.WorkOrderBuilder;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.util.*;

/**
 * Created by JohnRK on 14/06/2016.
 */
public class InMemoryWorkOrderQueue implements WorkOrderQueue {

    // A simple collection to store all the work orders that are currently live in the system
    // The list is not sorted or prioritised in any way, it is there to facilitate easy recalculation of
    // work order ranks
    private ArrayList<WorkOrder> workOrdersList = new ArrayList<>();

    // A sensible initial capacity of 100, default is 11
    private static final int INITIAL_CAPACITY = 100;

    // Instantiating a PriorityQueue with Initial capacity and a comparator which ensures the queue is ordered correctly
    private PriorityQueue<WorkOrder> workOrderQueue = new PriorityQueue<>(INITIAL_CAPACITY, new WorkOrderComparator());


    /**
     * Enqueues the specified work order.  If the queue already contains a work order with
     * the same requestor id, an exception is propogated back to the client (409 - Conflict)
     *
     * @param workOrder
     * @return
     */
    @Override
    public synchronized boolean enqueueWorkOrder(WorkOrder workOrder) throws Exception {
        // We just add a work order to the work orders list, and then we
        // perform a rerank of the Queue, ensuring the new object is present
        if (!workOrdersList.contains(workOrder)) {
            workOrdersList.add(workOrder);
            this.rerankQueue(DateTime.now());
            return true;
        } else {
            // Throw an exception back to the client that the requestor id already exists in the queue
            throw new ResourceAlreadyExistsException("Requestor ID", workOrder.getRequestorId());
        }
    }

    // TODO Implement Optional here
    /**
     * Dequeues the top ranked work order in the queue and returns it to the client
     *
     * @return
     */
    @Override
    public synchronized WorkOrder dequeueWorkOrder() throws Exception  {
        // First we perform a rerank of the queue to ensure that the elements of the queue are properly
        // ranked at the time that a dequeue operation is being performed.
        this.rerankQueue(DateTime.now());
        WorkOrder topWo = this.workOrderQueue.peek();

        // Ensure it is moved from the working list as well
        this.workOrdersList.remove(topWo);

        // Return to the caller
        return topWo;
    }

    // TODO Implement Optional here
    /**
     * Returns a sorted list of work order items
     * @return
     */
    @Override
    public synchronized List<WorkOrder> getSortedWorkOrderList() throws Exception {
        // Always ensure the queue is ranked correctly before retrieving
        this.rerankQueue(DateTime.now());

        // Get the contents of the queue as they are.  Priority queue does not return the array as sorted,
        // we need to sort it ourselves.
        WorkOrder [] workOrders = this.workOrderQueue.toArray(new WorkOrder[this.workOrderQueue.size()]);

        // Convert into a list
        ArrayList<WorkOrder> workOrdersAL = new ArrayList<WorkOrder>(Arrays.asList(workOrders));

        // Sort the list using our custom comparator and return it
        // Need another test comment here
        Collections.sort(workOrdersAL, new WorkOrderComparator());
        return workOrdersAL;
    }

    /**
     * Removes a workoder from the queue.
     *
     * If the specified work order cannot be found, an exception is thrown back to
     * the client (404 - Not Found)
     *
     * @param workOrder
     * @return
     */
    @Override
    public synchronized boolean removeWorkOrder(WorkOrder workOrder) throws Exception {
        if (workOrdersList.contains(workOrder)) {
            // Remove the work order from the working list
            workOrdersList.remove(workOrder);
            if(this.workOrderQueue.contains(workOrder)){
                // Remove it from the queue also.  No need to rerank beforehand
                this.workOrderQueue.remove(workOrder);
            }
            // This is another test comment
            return true;
        } else  {
            throw new ResourceNotFoundException("Requestor ID", workOrder.getRequestorId());
        }
    }

    /**
     * Returns the index of the specified Work Order in the queue.
     * If the specified work order cannot be found, an exception is thrown back to
     * the client (404 - Not Found)
     *
     * @param workOrder
     * @return
     */
    @Override
    public synchronized long getPositionOfID(WorkOrder workOrder) throws Exception {
        if(!this.getSortedWorkOrderList().contains(workOrder)){
            throw new ResourceNotFoundException("Requestor ID", workOrder.getRequestorId());
        }
        return this.getSortedWorkOrderList().indexOf(workOrder);
    }

    /**
     * Returns the Average Wait time in seconds of all the items in the queue.
     *
     * @return
     */
    @Override
    public synchronized long getAverageWaitTime(DateTime currentTime) throws Exception {
        List<WorkOrder> workOrders = getCopyOfWorkOrderListWithCalculatedWaitingTime(currentTime);

        // Test comment here

        if(workOrders.size() > 0) {
            long totalTimeInSeconds = workOrders
                    .stream()
                    .parallel()
                    .map(workOrder -> workOrder.getWaitTimeInSeconds())
                    .reduce(0, Integer::sum);
            return totalTimeInSeconds/workOrders.size();
        } else {
            return 0;
        }
    }

    /**
     * Takes a work order object and calculates the number of seconds it's been in the queue
     * (by subtracting the time it was inserted into the queue from the current time)
     * Depending on the type of work order it is, it will use the number of seconds to generate
     * a rank.  A new work order object is then constructed and the rank is applied to that new object
     * and returneed.
     *
     * @param workOrder
     * @param now
     */
    private WorkOrder determineRank(WorkOrder workOrder, DateTime now) {
        Seconds seconds = Seconds.secondsBetween(workOrder.getDateSubmitted(), now);
        double rank = 0;
        switch (workOrder.getWorkOrderType()) {
            case Normal:
                rank = seconds.getSeconds();
                break;
            case Priority:
                // Apply the priority formula to the number of seconds: max(3, n log n), using base e
                rank = Math.max(3, (seconds.getSeconds() * Math.log(seconds.getSeconds())));
                break;
            case VIP:
                // Apply the VIP formula to the number of seconds: max(42, n log n), using base e
                rank = Math.max(4, ((seconds.getSeconds() * 2) * Math.log(seconds.getSeconds())));
                break;
            case ManagementOverride:
                // Just set the rank at the number of seconds, as a mgmt override ticket it will be ranked ahead of the
                // other class types, but ranked within it's own class using the number of seconds
                rank = seconds.getSeconds();
                break;
            default:
        }
        // Workorders are immutable, so reconstruct the work order with existing values, but adding
        // in new values for rank and wait time in seconds
        WorkOrder newWo = new WorkOrderBuilder()
                .setRequestorId(workOrder.getRequestorId())
                .setDateSubmitted(workOrder.getDateSubmitted())
                .setIsMgmtOveride(workOrder.getIsMgmtOveride())
                .setWorkOrderType(workOrder.getWorkOrderType())
                .setRank(rank)
                .setWaitTimeInSeconds(seconds.getSeconds())
                .createWorkOrder();

        return newWo;
    }

    /**
     *  Method that clears the current queue, re-calculates the rank of the elements in the
     *  workOrderList.  It will then repopulate the queue with the contents of the workOrderList
     */
    private void rerankQueue(DateTime now){
        //DateTime now = DateTime.now();

        // First of all clear the queue
        this.workOrderQueue.clear();

        // Prepare an arraylist to put newly reconstructed work orders into
        ArrayList<WorkOrder> copyList = new ArrayList<>();

        // Now iterate through the existing workOrderList and recalculate the rank
        // of each workorder based on the current time.  Each object will be reconstructed with
        // updated information, and this is then added to the copyList arraylist
        for (WorkOrder wo  : this.workOrdersList) {
            copyList.add(this.determineRank(wo, now));
        }

        // Set the workOrderList to be the copyList
        this.workOrdersList = copyList;

        // Now, add all the items in the workOrderList to the Queue
        // The queue will order them by the sorting rules specified in the
        // WorkOrderComparator.  Queue should now be re-ranked and available for access
        workOrderQueue.addAll(this.workOrdersList);
    }


    /**
     * Method to obtain a copy of the work order working list and calculate the waiting time for each work order object
     * based on a date time that is specified
     *
     * @param currentTime
     * @return
     */
    public List<WorkOrder> getCopyOfWorkOrderListWithCalculatedWaitingTime(DateTime currentTime){
        ArrayList<WorkOrder> copy = new ArrayList<>();
        for(WorkOrder wo : this.workOrdersList){
            Seconds seconds = Seconds.secondsBetween(wo.getDateSubmitted(), currentTime);
            WorkOrder woCopy = new WorkOrderBuilder()
                    .setRequestorId(wo.getRequestorId())
                    .setDateSubmitted(wo.getDateSubmitted())
                    .setIsMgmtOveride(wo.getIsMgmtOveride())
                    .setWorkOrderType(wo.getWorkOrderType())
                    .setRank(wo.getRank())
                    .setWaitTimeInSeconds(seconds.getSeconds())
                    .createWorkOrder();
            copy.add(woCopy);
        }
        return copy;
    }

    private String useLessFunctionThatDoesNothing(String nada){
        return nada;
    }
}
