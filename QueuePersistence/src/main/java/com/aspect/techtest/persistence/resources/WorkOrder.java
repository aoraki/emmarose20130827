package com.aspect.techtest.persistence.resources;

import org.joda.time.DateTime;

/**
 * A represention of a Work Order
 * This class is an immutable class with private final member variables that are
 * set once in the constructor.  There is a public getter for each field, although
 * these could be dispensed by making the fields "public final"
 *
 * Created by JohnRK on 14/06/2016.
 *
 *
 */
public class WorkOrder {

    private final long requestorId;
    private final DateTime dateSubmitted;
    private final double rank;
    private final WorkOrderType workOrderType;
    private final int isMgmtOveride;
    private final int waitTimeInSeconds;

    public WorkOrder(long requestorId, DateTime dateSubmitted, double rank, WorkOrderType workOrderType, int isMgmtOveride, int waitTimeInSeconds) {
        this.requestorId = requestorId;
        this.dateSubmitted = dateSubmitted;
        this.rank = rank;
        this.workOrderType = workOrderType;
        this.isMgmtOveride = isMgmtOveride;
        this.waitTimeInSeconds = waitTimeInSeconds;
    }

    public int getIsMgmtOveride() {
        return isMgmtOveride;
    }

    public int getWaitTimeInSeconds() {
        return waitTimeInSeconds;
    }

    public long getRequestorId() {
        return requestorId;
    }

    public DateTime getDateSubmitted() {
        return dateSubmitted;
    }

    public double getRank() {
        return rank;
    }

    public WorkOrderType getWorkOrderType() {
        return workOrderType;
    }

    @Override
    public String toString() {
        return "WorkOrder{" +
                "requestorId=" + requestorId +
                ", dateSubmitted=" + dateSubmitted +
                ", rank=" + rank +
                ", workOrderType=" + workOrderType +
                ", isMgmtOveride=" + isMgmtOveride +
                ", waitTimeInSeconds=" + waitTimeInSeconds +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkOrder workOrder = (WorkOrder) o;

        return requestorId == workOrder.requestorId;
    }

    @Override
    public int hashCode() {
        return (int) (requestorId ^ (requestorId >>> 32));
    }
}
